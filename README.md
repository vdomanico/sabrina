# Sabrina
***
Containerize
```
$ docker build -t sabrina .
```
Deploy on k8s
```
$ kubectl create -f app.yaml
$ kubectl get pods
$ kubectl get service sabrina-service
$ curl -X GET http://localhost:32617/greet

```
Delete on k8s
```
$ kubectl delete -f app.yaml
```
