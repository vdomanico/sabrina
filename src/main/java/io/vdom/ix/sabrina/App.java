package io.vdom.ix.sabrina;

import io.helidon.microprofile.server.Server;

public class App {

    private App() { }

    public static void main(final String[] args) {
        Server.create().start();
    }


}
