package io.vdom.ix.sabrina.controller;

import org.eclipse.microprofile.metrics.annotation.Timed;

import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collections;

@RequestScoped
@Path("/greet")
public class GreetingController {

    private static final JsonBuilderFactory JSON = Json.createBuilderFactory(Collections.emptyMap());

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getDefaultMessage() {
        return JSON.createObjectBuilder()
                .add("message", "Hello World")
                .build();
    }

}