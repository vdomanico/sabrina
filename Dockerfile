FROM maven:3.6-adoptopenjdk-15 as build
WORKDIR /app

ADD pom.xml .
RUN mvn package -DskipTests

ADD src src
RUN mvn package -DskipTests
RUN echo "done!"

FROM openjdk:15.0.2-slim-buster
WORKDIR /app

COPY --from=build /app/target/sabrina.jar ./
COPY --from=build /app/target/libs ./libs

CMD ["java", "-jar", "sabrina.jar"]
EXPOSE 8080